class Product {
    constructor(id, name, quality, saleDate, isDelete)
    {
        this.id = id;
        this.name = name;
        this.quality = quality;
        this.saleDate = saleDate;
        this.isDelete = isDelete;
    }
    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
    }

    getName() {
        return this.id;
    }

    setName(name) {
        this.name = name;
    }

    getCategoryId() {
        return this.categoryId;
    }

    setCategory(categoryId) {
        this.categoryId = categoryId;
    }

    getSaleDate() {
        return this.saleDate;
    }

    setSaleDate(saleDate) {
        this.saleDate = saleDate;
    }

    getQuality() {
        return this.quality;
    }

    setQuality(quality) {
        this.quality = quality;
    }

    getIsDelete() {
        return this.idDelete;
    }

    setIsDelete(idDelete) {
        this.saleDate = idDelete;
    }
}