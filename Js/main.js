class Product {
    constructor(id, name, quality, categoryId, saleDate, isDelete) {
        this.id = id;
        this.name = name;
        this.quality = quality;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.isDelete = isDelete;
    }

    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
    }

    getName() {
        return this.id;
    }

    setName(name) {
        this.name = name;
    }

    getCategoryId() {
        return this.categoryId;
    }

    setCategory(categoryId) {
        this.categoryId = categoryId;
    }

    getSaleDate() {
        return this.saleDate;
    }

    setSaleDate(saleDate) {
        this.saleDate = saleDate;
    }

    getQuality() {
        return this.quality;
    }

    setQuality(quality) {
        this.quality = quality;
    }

    getIsDelete() {
        return this.isDelete;
    }

    setIsDelete(isDelete) {
        this.isDelete = isDelete;
    }
}

/**
 * Create list product have 10 product
 * 
 * @returns array object product
 */
function listProduct() {
    listProduct = [];
    for (let i = 0; i < 10; i++) {
        let product = new Product(i, 'name' + i, i * 10,i, '2025-04-26', false);
        listProduct.push(product);
    }
    return listProduct;
}
/**
 * Fiter product by id
 * 
 * @param array listProduct
 * @param interger idProduct
 * @returns array object product
 */
function fiterProductById(listProduct, idProduct) {
    let result = [];
    listProduct.forEach(product => {
        if (product.getId() === idProduct) {
            result.push(product)
        }
    });
    return result;
}

/**
 * Fiter product by id use Es6
 * 
 * @param array listProduct
 * @param interger idProduct
 * @returns array object product
 */
function fiterProductByIdEs6(listProduct, idProduct) {
    return listProduct.find((product) => {
        return product.getId() === idProduct;
    });
}

/**
* Fiter product with quality and isDelete

 * @param array listProduct
 * @returns array object product
 */
function fiterProductByQuality(listProduct) {
    let result = [];
    listProduct.forEach(product => {
        if (product.quality > 0 && !product.getIsDelete()) {
            result.push(product);
        }
    });
    return result;
}

/**
* Fiter product with quality and isDelete use Es6

 * @param array listProduct
 * @returns array object product
 */
function fiterProductByQualityEs6(listProduct) {
   return listProduct.filter((product) => {
        return product.quality > 0 && !product.getIsDelete();
   });
}

/**
 * Fiter product with saleDate, isDelete
 * 
 * @param array listProduct
 * @returns array object product
 */
function fiterProductSaleDate(listProduct) {
    let result = []; 
    let day = new Date().getTime();
    listProduct.forEach(product => {
            let saleDate = new Date(product.getSaleDate()).getTime();
    
        if (day < saleDate && !product.getIsDelete() ) {
            result.push(product);
        }
    });
    return result;
}

/**
 * Fiter product with saleDate and isDelete use Es6
 * 
 * @param array listProduct
 * @returns array object product
 */
function fiterProductSaleDateEs6(listProduct) {
     
    let timeCurrentDay = new Date().getTime();

    return listProduct.filter((product) => {
        let timeSaleDate = new Date(product.getSaleDate()).getTime();
         return timeCurrentDay < timeSaleDate &&  !product.getIsDelete()
    }); 
}

/**
 * Get total quality product has not been deleted
 * @param array listProduct
 * @returns interger
 */
function totalProduct(listProduct) {
    let totalQualityProduct = 0;
    listProduct.forEach(product => {
        if (!product.isDelete)
        totalQualityProduct += product.getQuality();
    });
    return totalQualityProduct;
}

/**
 * Get total quality product has not been deleted user Es6
 * @param array listProduct
 * @returns interger
 */
function totalProductEs6(listProduct) {
   return listProduct.reduce((totalQualityProduct, product) => {
            return totalQualityProduct + product.getQuality();
   }, 0);
}

/**
 * Check have product by categoryId
 * 
 * @param array listProduct
 * @param interger categoryId
 * @returns boole
 */
function isHaveProductInCategory(listProduct, categoryId) {
    let isHaveProduct = false;
    listProduct.forEach(product => {
        if (product.getCategoryId() === categoryId) {
            isHaveProduct = true;
        }
    });
    return isHaveProduct;
}

/**
 * Check have product by categoryId user Es6
 * 
 * @param array listProduct 
 * @param interger categoryId 
 * @returns boole
 */
function isHaveProductInCategoryEs6(listProduct, categoryId) {
    let isHaveProduct = listProduct.find((product) => {
        return product.getCategoryId() === categoryId;
    });

    if (typeof isHaveProduct === undefined)
        return false;
    return true;
}

/**
 * Fiter product by saleDate, quality
 * @param array listProduct 
 * @returns array object product
 */
 function fiterProductBySaleDate(listProduct) {
    let result = []; 
    let timeCurrentDay = new Date().getTime();
    listProduct.forEach(product => {
        let timeSaleDate = new Date(product.getSaleDate()).getTime();
        if (timeCurrentDay < timeSaleDate  && product.getQuality() > 0 ) {
        result.push(product);
        }
    });

    return result.map((product) => {
        return { id: product.getId(), name: product.getName() }
    });
}


/**
 * Fiter product by saleDate, quality use Es6
 * @param array listProduct 
 * @returns array object product
 */
function fiterProductBySaleDateEs6(listProduct) {
    let timeCurrentDay = new Date().getTime();
    result = listProduct.filter((product) => {
        let timeSaleDate = new Date(product.getSaleDate()).getTime();
        return TimeCurrentDay < timeSaleDate && product.getQuality() > 0
    });
    return result.map((product) => {
        return { id: product.getId(), name: product.getName() }
    })
}

let arrProduct = listProduct();

console.log(totalProductEs6(arrProduct));
console.log(isHaveProductInCategoryEs6(arrProduct,1))